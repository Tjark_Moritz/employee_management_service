import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {EmployeesComponent} from "./component/employees/employees.component";
import {PageNotFoundComponent} from "./component/pagenotfound/page-not-found.component";
import {QualificationsComponent} from "./component/qualifications/qualifications.component";
import {LoginPageComponent} from "./component/login-page/login-page.component";
import {AuthenticationGuardService} from "./service/authentication-guard.service";
import {QualificationDetailsComponent} from "./component/qualification-details/qualification-details.component";
import {EmployeeDetailsComponent} from "./component/employee-details/employee-details.component";

const routes: Routes = [
  {path:'login', component: LoginPageComponent},
  {path:'employees', component: EmployeesComponent, canActivate:[AuthenticationGuardService]},
  {path:'employees/add', component: EmployeeDetailsComponent, canActivate:[AuthenticationGuardService]},
  {path:'employees/edit/:employeeId', component: EmployeeDetailsComponent, canActivate:[AuthenticationGuardService]},
  {path:'qualifications', component: QualificationsComponent, canActivate:[AuthenticationGuardService]},
  {path:'qualifications/add', component: QualificationDetailsComponent, canActivate:[AuthenticationGuardService]},
  {path:'qualifications/edit/:designation', component: QualificationDetailsComponent, canActivate:[AuthenticationGuardService]},
  {path:'', redirectTo: '/login', pathMatch: 'full'},
  {path:'**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
