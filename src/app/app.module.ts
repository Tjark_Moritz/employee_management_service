import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { NavbarComponent } from './component/navbar/navbar.component';
import { MatDialogModule} from "@angular/material/dialog";
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MatTableModule} from "@angular/material/table";
import { MatCheckboxModule } from "@angular/material/checkbox";

import { AddEmployeeModalComponent } from "./component/addEmployeeModal/addEmployeeModal.component";
import { AddQualificationModalComponent } from './component/add-qualification-modal/add-qualification-modal.component';
import { EmployeesComponent } from './component/employees/employees.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './component/pagenotfound/page-not-found.component';
import { QualificationsComponent } from './component/qualifications/qualifications.component';
import { LoginPageComponent } from './component/login-page/login-page.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import { AlertboxComponent } from './component/alterbox/alterbox.component';
import { EmployeeDetailsComponent } from './component/employee-details/employee-details.component';
import { QualificationDetailsComponent } from './component/qualification-details/qualification-details.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AddEmployeeModalComponent,
    AppComponent,
    NavbarComponent,
    EmployeesComponent,
    PageNotFoundComponent,
    QualificationsComponent,
    AddQualificationModalComponent,
    LoginPageComponent,
    AlertboxComponent,
    EmployeeDetailsComponent,
    QualificationDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    MatCheckboxModule,
    HttpClientModule,
    AppRoutingModule,
    MatFormFieldModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddEmployeeModalComponent]
})
export class AppModule {
}
