import { Component, OnInit } from '@angular/core';
import {TokenCookieService} from "../../service/token-cookie.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  isLoggedIn(): boolean {
    return TokenCookieService.isTokenSet();
  }

  login(){
    this.router.navigate(['login']).then();
  }

  logout() {
    TokenCookieService.removeToken();
    this.router.navigate(['login']).then();
  }
}

