import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {BearerTokenService} from "../../service/bearerToken.service";
import {Router} from "@angular/router";
import {AlertService} from "../../service/alert.service";
import {TokenCookieService} from "../../service/token-cookie.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  username = "User"
  password = "password"

  constructor(
    private formBuilder: FormBuilder,
    private bearerTokenService: BearerTokenService,
    private router: Router,
    private alertService: AlertService
  ) {
    BearerTokenService.setBearerTokenFromCookie();
    if(this.bearerTokenService.bearerToken) {
      this.router.navigate(['employees']).then();
    }
  }

  ngOnInit() {
    if(this.bearerTokenService.bearerToken) {
      this.router.navigate(['employees']).then();
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.bearerTokenService.generateBearerToken(this.f['username'].value, this.f['password'].value).subscribe(
      res => {
      this.bearerTokenService.bearerToken = res;
      this.router.navigate(['employees']);
    }, error => {
        this.alertService.error("Username or password is incorrect")
        this.loading = false;
      })
  }

}
