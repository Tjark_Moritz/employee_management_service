import {Component, OnInit} from '@angular/core';
import {Employee} from "../../model/employee";
import {Qualification} from "../../model/qualification";
import {EmployeesService} from "../../service/employees.service"
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {QualificationService} from "../../service/qualification.service";
import {QualiEmployees} from "../../model/qualiEmployees";
import {AddQualificationModalComponent} from "../add-qualification-modal/add-qualification-modal.component";
import {BearerTokenService} from "../../service/bearerToken.service";
import {EmployeeDetails} from "../../model/employeeDetails";
import {AddEmployeeModalComponent} from "../addEmployeeModal/addEmployeeModal.component";

@Component({
  selector: 'app-qualification-details',
  templateUrl: './qualification-details.component.html',
  styleUrls: ['./qualification-details.component.css']
})
export class QualificationDetailsComponent implements OnInit {

  localQual: Qualification | undefined;
  localQualDetails: QualiEmployees | undefined;
  addEmpArray: Employee[] = [];
  delEmpArray: Employee[] = [];

  constructor(private httpClient: HttpClient, private empService: EmployeesService, private qualService: QualificationService, private route: ActivatedRoute, private dialog: MatDialog, private router:Router) {
    BearerTokenService.setBearerTokenFromCookie();
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    if(routeParams.keys.length > 0) {
      const givenDesignation: string = "" + routeParams.get('designation');
      if(givenDesignation != ""){
        this.getQualAndEmpByName(givenDesignation).then();
      }
    }
  }

  async getQualAndEmpByName(designation: string){
    this.localQual = new Qualification(designation);
    this.localQualDetails = await this.qualService.getQualificationEmployees(this.localQual).toPromise();
    this.sortQualEmp();
  }

  sortQualEmp(){
    if(this.localQualDetails){
      if(this.localQualDetails.employees){
        this.localQualDetails.employees.sort((obj1, obj2) => {
          // @ts-ignore
          if (obj1.lastName > obj2.lastName) {
            return 1;
          }
          // @ts-ignore
          if (obj1.lastName < obj2.lastName) {
            return -1;
          }
          return 0;
        });
      }
    }
  }

  async deleteQual(){
    // Check if local qual is defined
    if(this.localQual) {
      // Get confirmation from the user if he really want's to delete the quali
      if (confirm("Are you sure to delete " + this.localQual.designation + "?")) {
        if(this.localQualDetails && this.localQualDetails.employees){
          if(this.localQualDetails.employees.length >= 1){
            // Get the confirmation if the quali is still assigned to employees
            if(confirm(this.localQualDetails.employees.length + " Employees are assigned to: " + this.localQual.designation + ".\nDo you wan't to delete the qualification from all the employees?")) {
              for (let i = 0; i < this.localQualDetails.employees.length; i++) {
                await this.empService.deleteQualificationFromEmployee(this.localQualDetails.employees[i], this.localQual);
              }
            }
            else {
              return;
            }
          }

          await this.qualService.deleteQualification(this.localQual);
        }
      }
    }

    await new Promise(f => setTimeout(f, 100));
    await this.router.navigate(["qualifications"]);
  }

  async deleteEmp(employee: Employee){
    if(this.localQual){
      this.delEmpArray.push(employee);
      if(this.localQualDetails){
        if(this.localQualDetails.employees){
          let retour = this.localQualDetails.employees.findIndex(value => {
            return value == employee;
          })
          this.localQualDetails?.employees?.splice(retour, 1);
        }
      }
    }
    else {
      console.log("Kein Qual ausgewählt")
    }
    this.sortQualEmp();
  }

  cancel(){
    this.router.navigate(["qualifications"]);
  }

  async saveQual(){
    if(!this.localQual) {
      let designationName = (document.getElementById("qualDetDes") as HTMLInputElement).value;
      if(designationName == "") {
        alert("Designation name cannot be empty!");
        return;
      }

      this.localQual = new Qualification();
      this.localQual.designation = designationName;

      // Create, kein Update möglich
      // Checkt, ob eine Qualifikation mit dem Namen breits vorhanden ist
      this.qualService.getQualificationEmployees(this.localQual).subscribe(
        result => {
        },
        error => {
          if(this.localQual){
            this.qualService.createQualification(this.localQual);
          }
        }
      );
    }

    // Anpassen der hinzugefügten und gelöschten Qualifikationen um dopplungen zu vermeiden
    let delEmpArrayLen = this.delEmpArray.length
    for (let i = 0; i < delEmpArrayLen; i++) {
      let retour = this.addEmpArray.findIndex(value => {
        return value == this.delEmpArray[i];
      });
      if(retour >= 0){
        this.delEmpArray.splice(i, 1);
        this.addEmpArray.splice(retour, 1);
        delEmpArrayLen = this.delEmpArray.length;
        i = i - 1;
      }
    }

    // Qualifikationen anpassen
    for (let i = 0; i < this.delEmpArray.length; i++) {
      this.empService.deleteQualificationFromEmployee(this.delEmpArray[i], this.localQual);
    }

    for (let i = 0; i < this.addEmpArray.length; i++) {
      this.empService.addQualificationToEmployee(this.addEmpArray[i], this.localQual);
    }

    await new Promise(f => setTimeout(f, 100));
    this.router.navigate(["qualifications"]);
  }

  async openModal(){
    let modal = this.dialog.open(AddQualificationModalComponent, {data: this.localQual});
    let observable: Observable<any>;
    observable = modal.afterClosed();
    observable.subscribe(data => {
      if(data.data == undefined){
        return;
      }
      if(this.localQualDetails){
        let temp: Employee[];
        temp = data.data;
        this.addEmpArray = temp.filter(value => {

          if(this.localQualDetails?.employees){ // NOT UNDEF

            return this.localQualDetails.employees.find(v2 => {

              if(v2.id == value.id){ // wenn beide Qual gleich sind
                return true;
              }
              else {
                let retour = this.delEmpArray.findIndex(v3 => {
                  return v3 == value;
                });
                this.delEmpArray.splice(retour, 1);
                return retour >= 0;
              }
            }) == undefined;
          }
          else {
            return;
          }
        });
        this.localQualDetails.employees = data.data;
        this.sortQualEmp();
      }
    });
  }
}
