import {Component} from '@angular/core';
import {Qualification} from "../../model/qualification";
import {QualificationService} from "../../service/qualification.service";
import {BearerTokenService} from "../../service/bearerToken.service";

@Component({
  selector: 'app-qualifications',
  templateUrl: './qualifications.component.html',
  styleUrls: ['./qualifications.component.css']
})
export class QualificationsComponent{
  qualifications: Qualification[] = [];

  constructor(private qualiService: QualificationService) {
    BearerTokenService.setBearerTokenFromCookie();
    this.getQualifications();
  }

  getQualifications(){
    this.qualiService.getAllQualifications().subscribe((val) => {
      this.qualifications = val;
    });
  }
}
