import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQualificationModalComponent } from './add-qualification-modal.component';

describe('AddQualificationModalComponent', () => {
  let component: AddQualificationModalComponent;
  let fixture: ComponentFixture<AddQualificationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddQualificationModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQualificationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
