import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from "@angular/cdk/collections";
import {Qualification} from "../../model/qualification";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {QualificationService} from "../../service/qualification.service";
import {EmployeesService} from "../../service/employees.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Employee} from "../../model/employee";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
  selector: 'app-add-qualification-modal',
  templateUrl: './add-qualification-modal.component.html',
  styleUrls: ['./add-qualification-modal.component.css']
})
export class AddQualificationModalComponent implements OnInit {
  displayedColumns: string[] = ['select', 'lastName', 'firstName'];
  selection: SelectionModel<Employee>;

  allEmployees: Employee[];
  dataSource: MatTableDataSource<Employee>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private qualificationService: QualificationService,
              private employeeService: EmployeesService,
              private dialogRef: MatDialogRef<AddQualificationModalComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Qualification) {
  }
  ngOnInit() {
    this.qualificationService.getQualificationEmployees(this.data).subscribe(res => {
      this.selection = new SelectionModel<Employee>(true, res.employees);
    }, error => {
      this.selection = new SelectionModel<Employee>(true, [])
      console.log("There was an error getting the Employees for the given Qualification: ", error);
    })

    this.employeeService.getAllEmployees().subscribe(
      (res: Employee[]) => {
        this.allEmployees = res;
        this.dataSource = new MatTableDataSource<Employee>(this.allEmployees);
        this.dataSource.sort = this.sort;
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  close() {
    this.dialogRef.close({ event: 'close', data: undefined });
  }

  save() {
    this.dialogRef.close({ event: 'close', data: this.selection.selected });
  }

  OnChanged(event: MatCheckboxChange, row: Employee) {
    let temp = this.selection.selected.find(item => item.id === row.id)
    temp !== undefined ?
        this.selection.deselect(temp) : this.selection.select(row);
  }

  GetCheckedState(row: Employee): boolean{
    return this.selection.selected.some(item => item.id === row.id);
  }
}
