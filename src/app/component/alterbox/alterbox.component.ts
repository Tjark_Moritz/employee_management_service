import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../service/alert.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-alertbox',
  templateUrl: './alertbox.component.html',
  styleUrls: ['./alertbox.component.css']
})
export class AlertboxComponent implements OnInit {

  private subscription: Subscription;
  message: any;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.subscription = this.alertService.getAlert()
      .subscribe(alert => {
        if (alert.type === 'error')
          alert.cssClass = 'alert alert-danger';

        this.message = alert;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
