import {Component} from '@angular/core';
import { Employee } from "../../model/employee";
import { EmployeeDetails } from "../../model/employeeDetails";
import { EmployeesService } from "../../service/employees.service";
import {Qualification} from "../../model/qualification";
import {BearerTokenService} from "../../service/bearerToken.service";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent{
  employees: Employee[] = [];
  employeesDetails : EmployeeDetails[] = [];

  constructor(private empService: EmployeesService) {
    BearerTokenService.setBearerTokenFromCookie();
    this.getEmployees();
  }

  getEmployees(){
    this.empService.getAllEmployees().subscribe((val) => {
      this.employees = val;

      val.forEach((employee) => {
        this.empService.getEmployeeQualifications(employee).subscribe((det) => {
          this.employeesDetails.push(det);
        });
      });
    });
  }

  getQualificationString(qualifications : Qualification[]) : string {
    let result = "";

    qualifications.forEach((item, index) => {
      result += item.designation;
      if(index + 1 != qualifications.length)
        result += ", ";
    });

    return result;
  }
}
