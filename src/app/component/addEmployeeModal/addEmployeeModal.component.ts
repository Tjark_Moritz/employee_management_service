import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Employee} from "../../model/employee";
import {Qualification} from "../../model/qualification";
import {QualificationService} from "../../service/qualification.service";
import {EmployeesService} from "../../service/employees.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {SelectionModel} from "@angular/cdk/collections";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
  selector: 'app-addEmployeeModal',
  templateUrl: './addEmployeeModal.component.html',
  styleUrls: ['./addEmployeeModal.component.css']
})
export class AddEmployeeModalComponent implements OnInit {
  displayedColumns: string[] = ['select', 'designation'];
  selection: SelectionModel<Qualification> = new SelectionModel<Qualification>();

  allQualifications: Qualification[] = [];
  dataSource: MatTableDataSource<Qualification> = new MatTableDataSource<Qualification>();

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private qualificationService: QualificationService,
              private employeeService: EmployeesService,
              private dialogRef: MatDialogRef<AddEmployeeModalComponent>,
              @Inject(MAT_DIALOG_DATA) private data: Employee) {
  }
  ngOnInit() {
    this.employeeService.getEmployeeQualifications(this.data).subscribe(res => {
      this.selection = new SelectionModel<Qualification>(true, res.skillSet)
    }, error => {
      this.selection = new SelectionModel<Qualification>(true, [])
    })

    this.qualificationService.getAllQualifications().subscribe(
      (res: Qualification[]) => {
        this.allQualifications = res;
        this.dataSource = new MatTableDataSource<Qualification>(this.allQualifications);
        this.dataSource.sort = this.sort;
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  close() {
    this.dialogRef.close({ event: 'close', data: undefined });
  }

  save() {
    this.dialogRef.close({ event: 'close', data: this.selection.selected });
  }

  OnChanged(event: MatCheckboxChange, row: Qualification) {
    let temp = this.selection.selected.find(item => item.designation === row.designation)
    temp !== undefined ? this.selection.deselect(temp) : this.selection.select(row);
  }

  GetCheckedState(row: Qualification): boolean{
    return this.selection.selected.some(item => item.designation === row.designation);
  }
}
