import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Employee} from "../model/employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EmployeeDetails} from "../model/employeeDetails";
import {Qualification} from "../model/qualification";
import {BearerTokenService} from "./bearerToken.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  private urlPre = "/backend/employees"

  constructor(private httpClient: HttpClient, private bearerTokenService: BearerTokenService) {
  }

  getAllEmployees(): Observable<Employee[]>{
    return this.httpClient.get<Employee[]>(this.urlPre, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    });
  }

  saveEmployee(employee: Employee){
    this.httpClient.post(this.urlPre,{
        'lastName': employee.lastName,
        'firstName': employee.firstName,
        'street': employee.street,
        'postcode': employee.postcode,
        'city': employee.city,
        'phone': employee.phone
    },{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  getEmployee(employee: Employee): Observable<Employee>{
    return this.httpClient.get<Employee>(this.urlPre + `/${employee.id}`,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    });
  }

  updateEmployee(employee: Employee){
    this.httpClient.put(this.urlPre + `/${employee.id}`,employee,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  deleteEmployee(employee: Employee){
    this.httpClient.delete(this.urlPre + `/${employee.id}`,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  getEmployeeQualifications(employee: Employee): Observable<EmployeeDetails> {
    return this.httpClient.get<EmployeeDetails>(this.urlPre + `/${employee.id}/qualifications`, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    })
  }

  addQualificationToEmployee(employee: Employee, designation: Qualification){
    this.httpClient.post(this.urlPre + `/${employee.id}/qualifications`, designation, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  deleteQualificationFromEmployee(employee: Employee, designation: Qualification){
    this.httpClient.delete(this.urlPre + `/${employee.id}/qualifications`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.bearerTokenService.bearerToken?.access_token}`
        }),
      body: {
        'designation': designation.designation
      }
    }).toPromise();
  }
}
