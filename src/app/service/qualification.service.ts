import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BearerTokenService} from "./bearerToken.service";
import {Qualification} from "../model/qualification";
import {QualiEmployees} from "../model/qualiEmployees";

@Injectable({
  providedIn: 'root'
})
export class QualificationService {
  private urlPre = "/backend/qualifications"

  constructor(private httpClient: HttpClient, private bearerTokenService: BearerTokenService) {
  }

  getAllQualifications(): Observable<Qualification[]>{
      return this.httpClient.get<Qualification[]>(this.urlPre, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
      });
  }

  createQualification(qualification: Qualification){
    this.httpClient.post(this.urlPre, {
      "designation": qualification.designation
    },{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  deleteQualification(qualification: Qualification){
    this.httpClient.delete(this.urlPre,{
      body: {
        'designation': qualification.designation
      },
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    }).toPromise();
  }

  getQualificationEmployees(qualification: Qualification): Observable<QualiEmployees>{
     return this.httpClient.get<QualiEmployees>(this.urlPre + `/${qualification.designation}/employees`,{
      headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.bearerTokenService.bearerToken?.access_token}`)
    })
  }
}
