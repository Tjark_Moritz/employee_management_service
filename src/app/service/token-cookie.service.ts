import { Injectable } from '@angular/core';
import { CookieService} from "ngx-cookie-service";
import {BearerToken} from "../model/bearerToken";
import {BearerTokenService} from "./bearerToken.service";

@Injectable({
  providedIn: 'root'
})
export class TokenCookieService{
  private static tokenCookieName: string = "BearerTokenCookie";
  public static cookieService: CookieService;
  constructor() { }

  public static isTokenSet() : boolean{
    return TokenCookieService.cookieService.check(TokenCookieService.tokenCookieName);
  }

  public static setToken(bearerToken: BearerToken) {
    TokenCookieService.cookieService.set(TokenCookieService.tokenCookieName, JSON.stringify(bearerToken), bearerToken.expires_in);
  }

  public static getToken() : BearerToken {
    return <BearerToken>JSON.parse(TokenCookieService.cookieService.get(TokenCookieService.tokenCookieName));
  }

  public static removeToken() {
    TokenCookieService.cookieService.deleteAll(TokenCookieService.tokenCookieName);
  }
}
