import { Injectable } from '@angular/core';
import {Router, NavigationStart} from "@angular/router";
import {Observable, Subject, Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject = new Subject<any>();
  private subscription: Subscription;

  constructor(private router: Router) {
    this.subscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.clear();
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  error(message: string) {
    this.subject.next({ type: 'error', text: message });
  }

  clear() {
    this.subject.next('')
  }
}

