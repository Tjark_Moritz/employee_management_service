import {Component} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {TokenCookieService} from "./service/token-cookie.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   constructor(private cookieService: CookieService) {
     TokenCookieService.cookieService = cookieService;
  }
}
