import {Qualification} from "./qualification";

export class EmployeeDetails{
  constructor(public id?: number,
              public lastName?: string,
              public firstName?: string,
              public skillSet?: Qualification[]) {
  }
}
