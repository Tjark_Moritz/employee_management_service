// This has to exist because of questionable endpoint design, as the EmployeesForAQualificationDTO
// contains the designation, although to request that DTO, one has to already know the designation

import {Employee} from "./employee";

export class QualiEmployees {
  constructor(
    public designation?: string,
    public employees?: Employee[]
  ) {
  }
}


